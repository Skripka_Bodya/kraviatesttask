﻿using System.Threading.Tasks;
using KraviaDepartments.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KraviaDepartments.Controllers
{
    public class DataController : Controller
    {
        private DepartmentContext departmentContext;

        public DataController(DepartmentContext context) => departmentContext = context;

        public async Task<IActionResult> Index() => View(await departmentContext.Departments.ToListAsync());

        public IActionResult AddNewNote() => View();

        [HttpPost]
        public async Task<IActionResult> AddNewNote(Department department)
        {
            if (ModelState.IsValid)
            {
                departmentContext.Departments.Add(department);
                await departmentContext.SaveChangesAsync();
            }
            ViewBag["SomeTrouble"] = string.Format(ModelState.ValidationState + " Errors count = " + ModelState.ErrorCount);
            return RedirectToAction("Index", "Home");
        }
    }
}