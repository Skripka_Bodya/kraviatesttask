﻿using Microsoft.EntityFrameworkCore;

namespace KraviaDepartments.Models
{
    public class DepartmentContext : DbContext
    {
        public DbSet<Department> Departments { get; set; }

        public DepartmentContext(DbContextOptions<DepartmentContext> options) : base(options)
        {
        }
    }
}