﻿using System.ComponentModel.DataAnnotations;

namespace KraviaDepartments.Models
{
    public class Department
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "You should input Name Of Department")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Length of string 5 to 50 symbols")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required(ErrorMessage = "You should input Address of Your departmant")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Length of string 5 to 50 symbols")]
        [DataType(DataType.Text)]
        public string Address { get; set; }

        [Required(ErrorMessage = "You should input Name Of Director")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Length of string 5 to 50 symbols")]
        [DataType(DataType.Text)]
        public string HeadOfDepartment { get; set; }

        [Range(2, 10000, ErrorMessage = "Undefined number ")]
        [Required(ErrorMessage = "You should input Team Size")]
        public int TeamSize { get; set; }

        [Range(2, 10000, ErrorMessage = "Undefined number ")]
        [Required(ErrorMessage = "Please, enter average salary")]
        [DataType(DataType.Currency)]
        public double AverageSalary { get; set; }
    }
}